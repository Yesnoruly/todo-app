import React from 'react';
import more from "../../icons/more/More.svg";
import s from './Header.module.scss'

const Header = () => {
    return (
        <div className={s.header}>
            <h2 className={s.date}>Today</h2>
            <button className={s.more}><img src={more} alt=""/></button>
        </div>
    )
}

export default Header;