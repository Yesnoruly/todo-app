import React from 'react';
import EasyEdit, {Types} from 'react-easy-edit';
import s from './List.module.scss'
import unmarked from "../../icons/checkbox/Unmarked.svg";
import marked from "../../icons/checkbox/Marked.svg";
import del from "../../icons/delete/delete.svg";

const List = (props) => {
    return (
        <ul className={s.list}>
            {
                props.todos.map(t => {
                    return (
                        <li key={t.id} className={s.item}>

                            <EasyEdit
                                onSave={() => {}}
                                type={Types.TEXT}
                                saveButtonLabel="Save Me"
                                cancelButtonLabel="Cancel Me"
                                attributes={{ name: "awesome-input", id: 1}}
                                value={t.title}
                                saveOnBlur
                            />

                            <button onClick={
                                () => props.tasksSettings.setStatus(t.id, 1)} className={s.button}><img
                                src={0 === t.status ? unmarked : marked} alt="Button checkbox"/>
                            </button>

                            {t.status === 1 &&
                                <button onClick={() => props.tasksSettings.remove(t.id)} className={s.delete}><img
                                    src={del} alt="Delete icon"/>
                                </button>}
                        </li>
                    )
                })
            }
        </ul>
    )
}

export default List;