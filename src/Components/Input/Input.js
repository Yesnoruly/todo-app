import React from 'react';
import s from './Input.module.scss'

const Input = (props) => {
    return (
        <div className={s.input}>
            <input
                type="text"
                placeholder={'Enter task...'}
                value={props.input}
                onChange={props.handleInputChange}
                onKeyPress={props.handleInputKeyPress}
            />
        </div>
    )
}

export default Input;