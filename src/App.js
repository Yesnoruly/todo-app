import React, {useState} from "react";
import './App.scss';
import Header from "./Components/Header/Header";
import Input from "./Components/Input/Input";
import List from "./Components/List/List";

const todoModel = (id, title) => {
    return {
        id,
        title,
        status: 0, // 0 - in worked, 1 - done
    }
}

const App = () => {

    const [todos, setTodos] = useState([])
    const [input, setInput] = useState('')

    const handleInputChange = e => setInput(e.target.value)

    const tasksSettings = {
        remove: id => setTodos(todos.filter(t => t.id !== id)),

        add: title => setTodos([todoModel(todos.length, title), ...todos]),
        
        setStatus: (id, status) => setTodos(todos.map(t => {
            if(t.id === id) {
                t.status = status
                return t
            } else {
                return t
            }
        }))
    }

    const handleInputKeyPress = (e) => {
        if(e.key === "Enter" && input !== ''){
            tasksSettings.add(input)
            setInput('')
        }
    }

    return (
        <div className="App">

            <Header />

            <Input input={input} setInput={setInput} handleInputChange={handleInputChange} handleInputKeyPress={handleInputKeyPress}/>

            <List todos={todos} tasksSettings={tasksSettings}/>

        </div>
    );
}

export default App;
